using System;
using Xunit;
using System.Collections.Generic;
using MM = Models.Models;
using DTO = Models.DTO;
using MP = Models.Profiles;
using Models.LINQQueryResults;
using _01_linq;
using System.Linq;
using FakeItEasy;
using Server.Interfaces;
using Server.UnitsOfWork;
using DataAccess;
using Microsoft.EntityFrameworkCore;
using Server.Services;
using AutoMapper;

namespace Project.UnitTests
{
    public class ProjectQueryTest
    {
        protected readonly IUnitOfWork _unitFake;
        protected readonly IUnitOfWork _unitReal;
        protected readonly IMapper _mapper;

        public ProjectQueryTest()
        {
            _unitFake = A.Fake<IUnitOfWork>();

            var options = new DbContextOptionsBuilder<ProjectContext>()
                            .UseInMemoryDatabase(databaseName: "UnitTestDatabase")
                            .Options;
            var context = new ProjectContext(options);
            context.Database.EnsureCreated();
            _unitReal = new UnitOfWork(context);

            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MP.UserProfile>();
                cfg.AddProfile<MP.TaskProfile>();
                cfg.AddProfile<MP.TeamProfile>();
                cfg.AddProfile<MP.ProjectProfile>();
            });
            _mapper = config.CreateMapper();
        }
        [Fact]
        public void Task01_WhenProjectHas3Tasks_Then3()
        {
            MM.Project p1 = new MM.Project
            {
                Author = new MM.User { Id = 1 },
                Tasks = new List<MM.Task>
                {
                    new MM.Task{Id = 1},
                    new MM.Task{Id = 2},
                    new MM.Task{Id = 3},
                }
            };
            MM.Project p2 = new MM.Project
            {
                Author = new MM.User { Id = 2 },
                Tasks = new List<MM.Task>
                {
                    new MM.Task{Id = 4},
                    new MM.Task{Id = 5},
                    new MM.Task{Id = 6},
                }
            };
            List <MM.Project> projects = new List<MM.Project>() { p1, p2 };

            var result = Solution.task_01(projects, 1);
            Assert.Equal(3, result[p1]);
        }

        [Fact]
        public void Task01_WhenAuthorIsNotFound_ThenEmpty()
        {
            MM.Project p1 = new MM.Project
            {
                Author = new MM.User { Id = 1 },
                Tasks = new List<MM.Task>
                {
                    new MM.Task{Id = 1},
                    new MM.Task{Id = 2},
                    new MM.Task{Id = 3},
                }
            };
            MM.Project p2 = new MM.Project
            {
                Author = new MM.User { Id = 2 },
                Tasks = new List<MM.Task>
                {
                    new MM.Task{Id = 4},
                    new MM.Task{Id = 5},
                    new MM.Task{Id = 6},
                }
            };
            List<MM.Project> projects = new List<MM.Project>() { p1, p2 };

            var result = Solution.task_01(projects, 3);
            Assert.Empty(result);
        }

        [Fact]
        public void Task02_ReturnsTheRightAmountOfTasks_IfTasksHaveNameLessThan45()
        {
            MM.User u1 = new MM.User { Id = 3 };
            MM.User u2 = new MM.User { Id = 4 };

            MM.Project p1 = new MM.Project
            {
                Author = new MM.User { Id = 1 },
                Tasks = new List<MM.Task>
                {
                    new MM.Task{Id = 1, Performer = u1, Name = "12345"},
                    new MM.Task{Id = 2, Performer = u2, Name = "aaaaa"},
                    new MM.Task{Id = 3, Performer = u1, Name = new String('1', 46)},
                }
            };
            MM.Project p2 = new MM.Project
            {
                Author = new MM.User { Id = 2 },
                Tasks = new List<MM.Task>
                {
                    new MM.Task{Id = 4, Performer = u1, Name = "1245"},
                    new MM.Task{Id = 5, Performer = u2, Name = "aacababa"},
                    new MM.Task{Id = 6, Performer = u2, Name = "a123aa"},
                }
            };
            List<MM.Project> projects = new List<MM.Project>() { p1, p2 };

            var result1 = Solution.task_02(projects, u1.Id);
            var result2 = Solution.task_02(projects, u2.Id);
            Assert.Equal(2, result1.Count());
            Assert.Equal(3, result2.Count());
        }

        [Fact]
        public void Task02_WhenNoTaskFound_ThenEmpty()
        {
            MM.User u1 = new MM.User { Id = 3 };
            MM.User u2 = new MM.User { Id = 4 };

            MM.Project p1 = new MM.Project
            {
                Author = new MM.User { Id = 1 },
                Tasks = new List<MM.Task>
                {
                    new MM.Task{Id = 1, Performer = u2, Name = "12345"},
                    new MM.Task{Id = 2, Performer = u2, Name = "aaaaa"},
                    new MM.Task{Id = 3, Performer = u2, Name = new String('1', 46)},
                }
            };
            MM.Project p2 = new MM.Project
            {
                Author = new MM.User { Id = 2 },
                Tasks = new List<MM.Task>
                {
                    new MM.Task{Id = 4, Performer = u2, Name = "1245"},
                    new MM.Task{Id = 5, Performer = u2, Name = "aacababa"},
                    new MM.Task{Id = 6, Performer = u2, Name = "a123aa"},
                }
            };
            List<MM.Project> projects = new List<MM.Project>() { p1, p2 };

            var result = Solution.task_02(projects, u1.Id);
            Assert.Empty(result);
        }

        [Fact]
        public void Task03_ReturnsTheRightAmountOfTasks_IfTasksFinishedThisYear()
        {
            MM.User u1 = new MM.User { Id = 3 };
            MM.User u2 = new MM.User { Id = 4 };

            MM.Project p1 = new MM.Project
            {
                Author = new MM.User { Id = 1 },
                Tasks = new List<MM.Task>
                {
                    new MM.Task{Id = 1, Performer = u1, Name = "12345", FinishedAt = DateTime.Now},
                    new MM.Task{Id = 2, Performer = u2, Name = "aaaaa"},
                    new MM.Task{Id = 3, Performer = u1, Name = new String('1', 46), FinishedAt = DateTime.Now},
                }
            };
            MM.Project p2 = new MM.Project
            {
                Author = new MM.User { Id = 2 },
                Tasks = new List<MM.Task>
                {
                    new MM.Task{Id = 4, Performer = u1, Name = "1245", FinishedAt = DateTime.Now},
                    new MM.Task{Id = 5, Performer = u2, Name = "aacababa", FinishedAt = DateTime.Now},
                    new MM.Task{Id = 6, Performer = u2, Name = "a123aa", FinishedAt = DateTime.Now},
                }
            };
            List<MM.Project> projects = new List<MM.Project>() { p1, p2 };

            var result1 = Solution.task_03(projects, u1.Id);
            var result2 = Solution.task_03(projects, u2.Id);
            Assert.Equal(3, result1.Count());
            Assert.Equal(2, result2.Count());
        }

        [Fact]
        public void Task03_WhenNoTasksFinishedThisYear_ThenEmpty()
        {
            MM.User u1 = new MM.User { Id = 3 };
            MM.User u2 = new MM.User { Id = 4 };

            MM.Project p1 = new MM.Project
            {
                Author = new MM.User { Id = 1 },
                Tasks = new List<MM.Task>
                {
                    new MM.Task{Id = 1, Performer = u1, Name = "12345", FinishedAt = DateTime.Now.AddYears(-2)},
                    new MM.Task{Id = 2, Performer = u2, Name = "aaaaa"},
                    new MM.Task{Id = 3, Performer = u1, Name = new String('1', 46)},
                }
            };
            MM.Project p2 = new MM.Project
            {
                Author = new MM.User { Id = 2 },
                Tasks = new List<MM.Task>
                {
                    new MM.Task{Id = 4, Performer = u1, Name = "1245", FinishedAt = DateTime.Now.AddYears(-6)},
                    new MM.Task{Id = 5, Performer = u2, Name = "aacababa", FinishedAt = DateTime.Now},
                    new MM.Task{Id = 6, Performer = u2, Name = "a123aa", FinishedAt = DateTime.Now},
                }
            };
            List<MM.Project> projects = new List<MM.Project>() { p1, p2 };

            var result = Solution.task_03(projects, u1.Id);
            Assert.Empty(result);
        }

        [Fact]
        public void Task04_WhenOneTeamWith3UsersThatAreMoreThan10YearsOld_Then3Users()
        {
            MM.User u1 = new MM.User { Id = 1, TeamId = 1, BirthDay = DateTime.Now.AddYears(-11) };
            MM.User u2 = new MM.User { Id = 2, TeamId = 1, BirthDay = DateTime.Now.AddYears(-11) };
            MM.User u3 = new MM.User { Id = 3, TeamId = 2, BirthDay = DateTime.Now.AddYears(-11) };
            MM.User u4 = new MM.User { Id = 4, TeamId = 1, BirthDay = DateTime.Now.AddYears(-11) };
            MM.User u5 = new MM.User { Id = 5, TeamId = 2, BirthDay = DateTime.Now.AddYears(-6)  };
            MM.User u6 = new MM.User { Id = 6, TeamId = 2, BirthDay = DateTime.Now.AddYears(-11) };

            MM.Team t1 = new MM.Team { Id = 1 };
            MM.Team t2 = new MM.Team { Id = 2 };

            MM.Project p1 = new MM.Project
            {
                Author = u1,
                Team = t1,
                Tasks = new List<MM.Task>
                {
                    new MM.Task{Id = 1, Performer = u2, Name = "12345", FinishedAt = DateTime.Now},
                    new MM.Task{Id = 2, Performer = u3, Name = "aaaaa"}
                }
            };
            MM.Project p2 = new MM.Project
            {
                Author = u4,
                Team = t2,
                Tasks = new List<MM.Task>
                {
                    new MM.Task{Id = 4, Performer = u5, Name = "1245", FinishedAt = DateTime.Now},
                    new MM.Task{Id = 5, Performer = u6, Name = "aacababa", FinishedAt = DateTime.Now},
                    new MM.Task{Id = 6, Performer = u1, Name = "a123aa", FinishedAt = DateTime.Now}
                }
            };
            List<MM.Project> projects = new List<MM.Project>() { p1, p2 };

            var result1 = Solution.task_04(projects);

            Assert.Equal(3, result1.Sum(t => t.Users.Count()));
        }

        [Fact]
        public void Task04_WhenNoTeamWereFound_ThenEMpty()
        {
            MM.User u1 = new MM.User { Id = 1, TeamId = 1, BirthDay = DateTime.Now.AddYears(-9) };
            MM.User u2 = new MM.User { Id = 2, TeamId = 1, BirthDay = DateTime.Now.AddYears(-5) };
            MM.User u3 = new MM.User { Id = 3, TeamId = 2, BirthDay = DateTime.Now.AddYears(-5) };
            MM.User u4 = new MM.User { Id = 4, TeamId = 1, BirthDay = DateTime.Now.AddYears(-7) };
            MM.User u5 = new MM.User { Id = 5, TeamId = 2, BirthDay = DateTime.Now.AddYears(-6) };
            MM.User u6 = new MM.User { Id = 6, TeamId = 2, BirthDay = DateTime.Now.AddYears(-1) };

            MM.Team t1 = new MM.Team { Id = 1 };
            MM.Team t2 = new MM.Team { Id = 2 };

            MM.Project p1 = new MM.Project
            {
                Author = u1,
                Team = t1,
                Tasks = new List<MM.Task>
                {
                    new MM.Task{Id = 1, Performer = u2, Name = "12345", FinishedAt = DateTime.Now},
                    new MM.Task{Id = 2, Performer = u3, Name = "aaaaa"}
                }
            };
            MM.Project p2 = new MM.Project
            {
                Author = u4,
                Team = t2,
                Tasks = new List<MM.Task>
                {
                    new MM.Task{Id = 4, Performer = u5, Name = "1245", FinishedAt = DateTime.Now},
                    new MM.Task{Id = 5, Performer = u6, Name = "aacababa", FinishedAt = DateTime.Now},
                    new MM.Task{Id = 6, Performer = u1, Name = "a123aa", FinishedAt = DateTime.Now}
                }
            };
            List<MM.Project> projects = new List<MM.Project>() { p1, p2 };

            var result = Solution.task_04(projects);

            Assert.Empty(result);
        }

        [Fact]
        public void Task05_WhenUserHas2Tasks_Then2()
        {
            MM.User u1 = new MM.User { Id = 1 };
            MM.User u2 = new MM.User { Id = 2 };
            MM.User u3 = new MM.User { Id = 3 };
            MM.User u4 = new MM.User { Id = 4 };

            MM.Project p1 = new MM.Project
            {
                Author = u1,
                Tasks = new List<MM.Task>
                {
                    new MM.Task{Id = 1, Performer = u3, Name = "12345"},
                    new MM.Task{Id = 2, Performer = u2, Name = "aaaaa"},
                    new MM.Task{Id = 3, Performer = u1, Name = new String('1', 46)},
                }
            };
            MM.Project p2 = new MM.Project
            {
                Author = u2,
                Tasks = new List<MM.Task>
                {
                    new MM.Task{Id = 4, Performer = u1, Name = "1245"},
                    new MM.Task{Id = 5, Performer = u3, Name = "aacababa"},
                    new MM.Task{Id = 6, Performer = u4, Name = "a123aa"},
                }
            };
            List<MM.Project> projects = new List<MM.Project>() { p1, p2 };

            var result = Solution.task_05(projects);

            Assert.Equal(2, result.Where(u => u.User.Id == u1.Id).Sum(u => u.Tasks.Count()));
        }

        [Fact]
        public void Task05_WhenNoTasksFound_ThenTaskListIsEmpty()
        {
            MM.User u1 = new MM.User { Id = 1 };
            MM.User u2 = new MM.User { Id = 2 };
            MM.User u3 = new MM.User { Id = 3 };
            MM.User u4 = new MM.User { Id = 4 };

            MM.Project p1 = new MM.Project
            {
                Author = u1,
                Tasks = new List<MM.Task>
                {
                    new MM.Task{Id = 1, Performer = u3, Name = "12345"},
                    new MM.Task{Id = 2, Performer = u2, Name = "aaaaa"},
                    new MM.Task{Id = 3, Performer = u3, Name = new String('1', 46)},
                }
            };
            MM.Project p2 = new MM.Project
            {
                Author = u2,
                Tasks = new List<MM.Task>
                {
                    new MM.Task{Id = 4, Performer = u2, Name = "1245"},
                    new MM.Task{Id = 5, Performer = u3, Name = "aacababa"},
                    new MM.Task{Id = 6, Performer = u4, Name = "a123aa"},
                }
            };
            List<MM.Project> projects = new List<MM.Project>() { p1, p2 };

            var result = Solution.task_05(projects);

            Assert.Empty(result.Where(u => u.User.Id == u1.Id).FirstOrDefault().Tasks);
        }

        [Fact]
        public void Task06_JustCheckIfResultOfQueryAreCorrect()
        {
            MM.User u1 = new MM.User { Id = 1 };
            MM.User u2 = new MM.User { Id = 2 };

            MM.Project p1 = new MM.Project
            {
                Id = 1,
                Author = u1,
                Tasks = new List<MM.Task>
                {
                    new MM.Task{Id = 1, Performer = u1, State = 2, FinishedAt = DateTime.Now, CreatedAt = DateTime.Now.AddYears(-1)},
                    new MM.Task{Id = 2, Performer = u2, State = 2, FinishedAt = DateTime.Now, CreatedAt = DateTime.Now.AddYears(-2)},
                    new MM.Task{Id = 3, Performer = u1, State = 2, FinishedAt = DateTime.Now, CreatedAt = DateTime.Now.AddYears(-5)},
                }
            };
            MM.Project p2 = new MM.Project
            {
                Id = 2,
                Author = u2,
                Tasks = new List<MM.Task>
                {
                    new MM.Task{Id = 4, Performer = u2, State = 2, FinishedAt = DateTime.Now, CreatedAt = DateTime.Now.AddYears(-2)},
                    new MM.Task{Id = 5, Performer = u1, State = 2, FinishedAt = DateTime.Now, CreatedAt = DateTime.Now.AddYears(-7)},
                    new MM.Task{Id = 6, Performer = u2, State = 1, FinishedAt = DateTime.Now, CreatedAt = DateTime.Now.AddYears(-15)},
                }
            };
            List<MM.Project> projects = new List<MM.Project>() { p1, p2 };

            var result = Solution.task_06(projects, u1.Id);
            var userInfo = result.FirstOrDefault();
            Assert.Equal(p1.Id, userInfo.LastProject.Id);
            Assert.Equal(0, userInfo.UnfinishedTasksCount);
            Assert.Equal(5, userInfo.LongestTask.Id);
        }

        [Fact]
        public void Task07_JustCheckIfResultOfQueryAreCorrect()
        {
            MM.User u1 = new MM.User { Id = 1 , TeamId = 1};
            MM.User u2 = new MM.User { Id = 2 , TeamId = 1};

            MM.Team t1 = new MM.Team { Id = 1 };

            MM.Project p1 = new MM.Project
            {
                Id = 1,
                Author = u1,
                Team = t1,
                Description = new string('1', 25),
                Tasks = new List<MM.Task>
                {
                    new MM.Task{Id = 1, Performer = u1, Name = new string('1', 20), Description = new string('1', 3)},
                    new MM.Task{Id = 2, Performer = u2, Name = new string('1', 2), Description = new string('1', 30)},
                }
            };
            MM.Project p2 = new MM.Project
            {
                Id = 2,
                Author = u2,
                Team = t1,
                Tasks = new List<MM.Task>
                {
                    new MM.Task{Id = 4, Performer = u2, State = 2, FinishedAt = DateTime.Now, CreatedAt = DateTime.Now.AddYears(-2)},
                    new MM.Task{Id = 5, Performer = u1, State = 2, FinishedAt = DateTime.Now, CreatedAt = DateTime.Now.AddYears(-7)},
                    new MM.Task{Id = 6, Performer = u2, State = 1, FinishedAt = DateTime.Now, CreatedAt = DateTime.Now.AddYears(-15)},
                }
            };
            List<MM.Project> projects = new List<MM.Project>() { p1, p2 };

            var result = Solution.task_07(projects);

            Assert.Equal(2, result.FirstOrDefault().LongestTask.Id);
            Assert.Equal(2, result.FirstOrDefault().ShortestTask.Id);
        }

        [Fact]
        public void WhenAddUserIsCalle_ThenRepositoryCreateHasHappendOnce()
        {
            A.CallTo(() => _unitFake.Set<MM.User>()).Returns(A.Fake<IRepository<MM.User>>());
            var _userRepository = _unitFake.Set<MM.User>();

            UserService _service = new UserService(_unitFake, A.Fake<IMapper>());
            DTO.UserDTO user = new DTO.UserDTO { Id = 1 };
            _service.AddUser(user);

            A.CallTo(() => _userRepository.Create(A<MM.User>._, null)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void WhenUpdateTaskIsCalle_ThenRepositoryUpdateHasHappendOnce()
        {
            A.CallTo(() => _unitFake.Set<MM.Task>()).Returns(A.Fake<IRepository<MM.Task>>());
            var _taskRepository = _unitFake.Set<MM.Task>();

            TaskService _service = new TaskService(_unitFake, A.Fake<IMapper>());
            DTO.TaskDTO task = new DTO.TaskDTO { Id = 1, State = 1 };
            _service.UpdateTask(task);

            A.CallTo(() => _taskRepository.Update(A<MM.Task>._, null)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void WhenUpdateUserIsCalle_ThenRepositoryUpdateHasHappendOnce()
        {
            A.CallTo(() => _unitFake.Set<MM.User>()).Returns(A.Fake<IRepository<MM.User>>());
            var _userRepository = _unitFake.Set<MM.User>();

            UserService _service = new UserService(_unitFake, A.Fake<IMapper>());
            DTO.UserDTO user = new DTO.UserDTO { Id = 1, TeamId = 1 };
            _service.UpdateUser(user);

            A.CallTo(() => _userRepository.Update(A<MM.User>._, null)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void GetUnfinishedTasks_WhenUserId1_Then2TasksReturned()
        {
            UserService _service = new UserService(_unitReal, _mapper);

            var tasks = _service.GetUnfinishedTasks(1);

            Assert.Equal(2, tasks.Count());
        }

        [Fact]
        public void GetUnfinishedTasks_WhenUserDoesNotExist_ThenEmpty()
        {
            UserService _service = new UserService(_unitReal, _mapper);

            var tasks = _service.GetUnfinishedTasks(10);

            Assert.Empty(tasks);
        }
    }
}
