﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Server.Services;
using Models.DTO;
using System.Net;
using Server.Interfaces;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        readonly IProjectService _service;
        public ProjectController(IProjectService service)
        {
            _service = service;
        }

        [HttpGet]
        [Route("read")]
        public ActionResult<IEnumerable<ProjectDTO>> Read()
        {
            return Ok(_service.GetProjects());
        }

        [HttpGet]
        [Route("read/{id}")]
        public ActionResult<ProjectDTO> Read(int id)
        {
            ProjectDTO project = _service.GetProjectById(id);

            if (project == null) return NotFound();
            return Ok(project);
        }

        [HttpPost]
        [Route("create")]
        public IActionResult Create([FromBody] ProjectDTO project)
        {
            if (_service.ExistsProject(project.Id)) return Conflict("Entity already exists");
            var createdProject = _service.AddProject(project);
            return StatusCode((int)HttpStatusCode.Created, createdProject);
        }

        [HttpPut]
        [Route("update")]
        public IActionResult Update([FromBody] ProjectDTO project)
        {
            _service.UpdateProject(project);
            return NoContent();
        }

        [HttpDelete]
        [Route("delete/{id}")]
        public IActionResult Delete(int id)
        {
            _service.DeleteProject(id);
            return NoContent();
        }
    }
}
