﻿using AutoMapper;
using MM = Models.Models;
using Models.DTO;
using Server.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Services
{
    public class TaskService : ITaskService
    {
        readonly IUnitOfWork _unitOfWork;
        readonly IRepository<MM.Project> _projectRepository;
        readonly IRepository<MM.Task> _taskRepository;
        readonly IRepository<MM.User> _userRepository;
        readonly IMapper _mapper;
        public TaskService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _projectRepository = _unitOfWork.Set<MM.Project>();
            _taskRepository = _unitOfWork.Set<MM.Task>();
            _userRepository = _unitOfWork.Set<MM.User>();
            _mapper = mapper;
        }

        public IEnumerable<TaskDTO> GetTasks()
        {
            var taskSet = _taskRepository.Get();

            return _mapper.Map<IEnumerable<TaskDTO>>(taskSet);
        }

        public TaskDTO GetTaskById(int id)
        {
            var taskSet = _taskRepository.Get(t => t.Id == id);

            if (!taskSet.Any()) return null;

            return _mapper.Map<TaskDTO>(taskSet.First());
        }

        public void AddTask(TaskDTO task)
        {
            MM.Task newTask = _mapper.Map<MM.Task>(task);
            MM.User taskPerformer = getTaskPerformer(task);
            MM.Project project = getTaskProject(task);

            newTask.Performer = taskPerformer;
            newTask.Project = project;
            _taskRepository.Create(newTask);
            _unitOfWork.SaveChanges();
        }

        public void UpdateTask(TaskDTO task)
        {
            MM.Task newTask = _mapper.Map<MM.Task>(task);
            MM.User taskPerformer = getTaskPerformer(task);
            MM.Project project = getTaskProject(task);

            newTask.Performer = taskPerformer;
            newTask.Project = project;
            _taskRepository.Update(newTask);
            _unitOfWork.SaveChanges();
        }

        public void DeleteTask(int id)
        {
            _taskRepository.Delete(id);
            _unitOfWork.SaveChanges();
        }

        MM.User getTaskPerformer(TaskDTO task)
        {
            var userSet = _userRepository.Get(u => u.Id == task.PerformerId);

            return userSet.FirstOrDefault();
        }

        MM.Project getTaskProject(TaskDTO task)
        {
            var projectSet = _projectRepository.Get(p => p.Id == task.ProjectId);

            return projectSet.FirstOrDefault();
        }

        public bool ExistsTask(int id)
        {
            var tasks = _taskRepository.Get(t => t.Id == id);
            return tasks.Any();
        }
    }
}
