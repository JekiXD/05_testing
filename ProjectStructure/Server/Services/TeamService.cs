﻿using AutoMapper;
using Server.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MM = Models.Models;
using Models.DTO;

namespace Server.Services
{
    public class TeamService : ITeamService
    {
        readonly IUnitOfWork _unitOfWork;
        readonly IRepository<MM.Team> _teamRepository;
        readonly IMapper _mapper;
        public TeamService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _teamRepository = _unitOfWork.Set<MM.Team>();
            _mapper = mapper;
        }

        public IEnumerable<TeamDTO> GetTeams()
        {
            var teamSet = _teamRepository.Get();

            return _mapper.Map<IEnumerable<TeamDTO>>(teamSet);
        }

        public TeamDTO GetTeamById(int id)
        {
            var teamSet = _teamRepository.Get(t => t.Id == id);

            if (!teamSet.Any()) return null;

            return _mapper.Map<TeamDTO>(teamSet.First());
        }

        public TeamDTO AddTeam(TeamDTO team)
        {
            MM.Team newTeam = _mapper.Map<MM.Team>(team);
            _teamRepository.Create(newTeam);
            _unitOfWork.SaveChanges();

            return _mapper.Map<TeamDTO>(newTeam);
        }

        public void UpdateTeam(TeamDTO team)
        {
            MM.Team newteam = _mapper.Map<MM.Team>(team);
            _teamRepository.Update(newteam);
            _unitOfWork.SaveChanges();
        }

        public void DeleteTeam(int id)
        {
            _teamRepository.Delete(id);
            _unitOfWork.SaveChanges();
        }

        public bool ExistsTeam(int id)
        {
            var teams = _teamRepository.Get(t => t.Id == id);
            return teams.Any();
        }
    }
}
