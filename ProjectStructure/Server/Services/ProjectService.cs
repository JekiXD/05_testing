﻿using AutoMapper;
using Server.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MM = Models.Models;
using Models.DTO;

namespace Server.Services
{
    public class ProjectService : IProjectService
    {
        readonly IUnitOfWork _unitOfWork;
        readonly IRepository<MM.Project> _projectRepository;
        readonly IRepository<MM.User> _userRepository;
        readonly IRepository<MM.Team> _teamRepository;
        readonly IRepository<MM.Task> _taskRepository;
        readonly IMapper _mapper;
        public ProjectService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _projectRepository = _unitOfWork.Set<MM.Project>();
            _userRepository = _unitOfWork.Set<MM.User>();
            _teamRepository = _unitOfWork.Set<MM.Team>();
            _taskRepository = _unitOfWork.Set<MM.Task>();
            _mapper = mapper;
        }

        public IEnumerable<ProjectDTO> GetProjects()
        {
            var projectSet = _projectRepository.Get();

            return _mapper.Map<IEnumerable<ProjectDTO>>(projectSet);
        }

        public ProjectDTO GetProjectById(int id)
        {
            var projectSet = _projectRepository.Get(p => p.Id == id);

            if (!projectSet.Any()) return null;

            return _mapper.Map<ProjectDTO>(projectSet.First());
        }

        public ProjectDTO AddProject(ProjectDTO project)
        {
            var newProject = _mapper.Map<MM.Project>(project);
            var author = getProjectAuthor(project);
            var team = getProjectTeam(project);
            var tasks = getProjectTasks(project);

            newProject.Author = author;
            newProject.Team = team;
            newProject.Tasks = tasks;
            _projectRepository.Create(newProject);
            _unitOfWork.SaveChanges();

            return _mapper.Map<ProjectDTO>(newProject);
        }

        public void UpdateProject(ProjectDTO project)
        {
            var newProject = _mapper.Map<MM.Project>(project);
            var author = getProjectAuthor(project);
            var team = getProjectTeam(project);
            var tasks = getProjectTasks(project);

            newProject.Author = author;
            newProject.Team = team;
            newProject.Tasks = tasks;
            _projectRepository.Update(newProject);
            _unitOfWork.SaveChanges();
        }

        public void DeleteProject(int id)
        {
            _projectRepository.Delete(id);
            _unitOfWork.SaveChanges();
        }

        public MM.User getProjectAuthor(ProjectDTO project)
        {
            var user = _userRepository.Get(u => u.Id == project.AuthorId);

            return user.FirstOrDefault();
        }

        public MM.Team getProjectTeam(ProjectDTO project)
        {
            var team = _teamRepository.Get(t => t.Id == project.TeamId);

            return team.FirstOrDefault();
        }

        public ICollection<MM.Task> getProjectTasks(ProjectDTO project)
        {
            var tasks = _taskRepository.Get(t => t.ProjectId == project.Id);
            return tasks.ToList();
        }

        public bool ExistsProject(int id)
        {
            var projects = _projectRepository.Get(p => p.Id == id);
            return projects.Any();
        }
    }
}
