﻿using Models.Abstract;
using Server.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DataAccess;

namespace Server.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        protected ProjectContext _context;

        public Repository(ProjectContext context)
        {
            _context = context;
        }
        public virtual IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null)
        {
            IQueryable<TEntity> query = _context.Set<TEntity>();

            if (filter != null) query = query.Where(filter);

            return query.ToList();
        }
        public virtual void Create(TEntity entity, string createdBy = null)
        {
            _context.Set<TEntity>().Add(entity);
        }
        public virtual void Update(TEntity entity, string modifiedBy = null)
        {
            _context.Set<TEntity>().Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
        }
        public virtual void Delete(object id)
        {
            TEntity entity = _context.Set<TEntity>().Find(id);
            Delete(entity);
        }
        public virtual void Delete(TEntity entity)
        {
            var dbSet = _context.Set<TEntity>();
            if (_context.Entry(entity).State == EntityState.Detached)
                dbSet.Attach(entity);
            dbSet.Remove(entity);
        }
    }
}
