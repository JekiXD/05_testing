using System;
using Xunit;
using System.Collections.Generic;
using MM = Models.Models;
using DTO = Models.DTO;
using Models.LINQQueryResults;
using _01_linq;
using System.Linq;
using FakeItEasy;
using Server.Interfaces;
using Server.UnitsOfWork;
using DataAccess;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Testing;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Text;
using Microsoft.Extensions.DependencyInjection;

namespace Project.IntegratedTests
{
    public class ProjectApiTest : IClassFixture<CustomWebApplicationFactory<Server.Startup>>
    {
        private readonly HttpClient _client;


        public ProjectApiTest(CustomWebApplicationFactory<Server.Startup> factory)
        {
            _client = factory.CreateClient();

        }
        [Fact]
        public async Task CreateProject_WhenNewProject_ThenCreated()
        {
            var project = new DTO.ProjectDTO { Id = 4 };
            var jsonString = JsonConvert.SerializeObject(project);

            var httpResponse = await _client.PostAsync("api/project/create", new StringContent(jsonString, Encoding.UTF8, "application/json"));

            await _client.DeleteAsync("api/project/delete/" + project.Id);

            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
        }
        [Fact]
        public async Task CreateProject_WhenProjectExists_ThenConflict()
        {
            var project = new DTO.ProjectDTO { Id = 4 };
            var jsonString = JsonConvert.SerializeObject(project);

            await _client.PostAsync("api/project/create", new StringContent(jsonString, Encoding.UTF8, "application/json"));
            var httpResponse = await _client.PostAsync("api/project/create", new StringContent(jsonString, Encoding.UTF8, "application/json"));

            await _client.DeleteAsync("api/project/delete/" + project.Id);

            Assert.Equal(HttpStatusCode.Conflict, httpResponse.StatusCode);
        }

        [Fact]
        public async Task DeleteUser_WhenUserDoesNotExist_ThenNotFound()
        {
            var user = new DTO.UserDTO { Id = 10 };

            var httpResponse = await _client.DeleteAsync("api/user/delete/" + user.Id);

            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }

        [Fact]
        public async Task DeleteUser_WhenUserExists_ThenNoContent()
        {
            var user = new DTO.UserDTO { Id = 10 };
            var jsonString = JsonConvert.SerializeObject(user);

            await _client.PostAsync("api/user/create", new StringContent(jsonString, Encoding.UTF8, "application/json"));
            var httpResponse = await _client.DeleteAsync("api/user/delete/" + user.Id);

            Assert.Equal(HttpStatusCode.NoContent, httpResponse.StatusCode);
        }

        [Fact]
        public async Task CreateTeam_WhenNewTeam_ThenCreated()
        {
            var team = new DTO.TeamDTO { Id = 4 };
            var jsonString = JsonConvert.SerializeObject(team);

            var httpResponse = await _client.PostAsync("api/team/create", new StringContent(jsonString, Encoding.UTF8, "application/json"));

            await _client.DeleteAsync("api/team/delete/" + team.Id);

            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
        }
        [Fact]
        public async Task CreateTeam_WhenTeamExists_ThenConflict()
        {
            var team = new DTO.TeamDTO { Id = 4 };
            var jsonString = JsonConvert.SerializeObject(team);

            await _client.PostAsync("api/team/create", new StringContent(jsonString, Encoding.UTF8, "application/json"));
            var httpResponse = await _client.PostAsync("api/team/create", new StringContent(jsonString, Encoding.UTF8, "application/json"));

            await _client.DeleteAsync("api/team/delete/" + team.Id);

            Assert.Equal(HttpStatusCode.Conflict, httpResponse.StatusCode);
        }

        [Fact]
        public async Task DeleteTask_WhenTaskDoesNotExist_ThenNotFound()
        {
            var task = new DTO.TaskDTO { Id = 10 };

            var httpResponse = await _client.DeleteAsync("api/task/delete/" + task.Id);

            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }

        [Fact]
        public async Task DeleteTask_WhenTaskExists_ThenNoContent()
        {
            var task = new DTO.TaskDTO { Id = 10 };
            var jsonString = JsonConvert.SerializeObject(task);

            await _client.PostAsync("api/task/create", new StringContent(jsonString, Encoding.UTF8, "application/json"));
            var httpResponse = await _client.DeleteAsync("api/task/delete/" + task.Id);

            Assert.Equal(HttpStatusCode.NoContent, httpResponse.StatusCode);
        }

        [Fact]
        public async Task CreateUser_WhenNewUser_ThenCreated()
        {
            var user = new DTO.UserDTO { Id = 7 };
            var jsonString = JsonConvert.SerializeObject(user);

            var httpResponse = await _client.PostAsync("api/user/create", new StringContent(jsonString, Encoding.UTF8, "application/json"));

            await _client.DeleteAsync("api/user/delete/" + user.Id);

            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
        }
        [Fact]
        public async Task CreateUser_WhenUserExists_ThenConflict()
        {
            var user = new DTO.UserDTO { Id = 7 };
            var jsonString = JsonConvert.SerializeObject(user);

            await _client.PostAsync("api/user/create", new StringContent(jsonString, Encoding.UTF8, "application/json"));
            var httpResponse = await _client.PostAsync("api/user/create", new StringContent(jsonString, Encoding.UTF8, "application/json"));

            await _client.DeleteAsync("api/user/delete/" + user.Id);

            Assert.Equal(HttpStatusCode.Conflict, httpResponse.StatusCode);
        }

        [Fact]
        public async Task GetUnfinishedTasks_WhenUserDoesNotExist_ThenNotFound()
        {
            var user = new DTO.UserDTO { Id = 11 };
            var httpResponse = await _client.GetAsync("api/user/unfinishedTasks/" + user.Id);

            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }

        [Fact]
        public async Task GetUnfinishedTasks_WesNotExist_ThenNotFound()
        {
            var user1 = new DTO.UserDTO { Id = 1 };

            var httpResponse = await _client.GetAsync("api/user/unfinishedTasks/" + user1.Id);
            var stringJson = await httpResponse.Content.ReadAsStringAsync();

            var tasks = JsonConvert.DeserializeObject<IEnumerable<DTO.TaskDTO>>(stringJson);

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.Equal(2, tasks.Count());
        }
    }
}
