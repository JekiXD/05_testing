﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.LINQQueryResults
{
    public class Task03Result
    {
        public int Id { set; get; }
        public string Name { set; get; }
    }
}
