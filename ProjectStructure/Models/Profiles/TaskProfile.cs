﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Models.DTO;
using Models.Models;

namespace Models.Profiles
{
    public class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<Task, TaskDTO>();

            CreateMap<TaskDTO, Task>();
        }
    }
}
