﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Models.DTO;
using Models.Models;

namespace Models.Profiles
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDTO>();

            CreateMap<ProjectDTO, Project>();
        }
    }
}
