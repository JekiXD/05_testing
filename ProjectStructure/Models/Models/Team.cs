﻿using Models.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Models
{
    public class Team : BaseEntity
    {
        public string Name { get; set; }
    }
}
