﻿using System;
using System.Collections.Generic;
using System.Text;
using Models.Models;
using System.Linq;
using Models.LINQQueryResults;

namespace _01_linq
{
    public static class Solution
    {
       public static Dictionary<Project, int> task_01(List<Project> data, int authorId)
        {
            Dictionary<Project, int> result = data
                                                .Where(p => p.Author.Id == authorId)
                                                .ToDictionary(p => p,
                                                              p => p.Tasks.Count());

            return result;
        }

        public static IEnumerable<Task> task_02(List<Project> data, int userId)
        {
            IEnumerable<Task> result = data
                                        .SelectMany(p => p.Tasks)
                                        .Where(t => t.Performer.Id == userId && t.Name.Length < 45);

            return result;
        }

        public static IEnumerable<Task03Result> task_03(List<Project> data, int userId)
        {
            IEnumerable<Task03Result> result = data
                                                .SelectMany(p => p.Tasks)
                                                .Where(t => t.Performer.Id == userId
                                                         && t.FinishedAt?.Year == DateTime.Now.Year)
                                                .Select(t => new Task03Result { Id = t.Id, Name = t.Name });

            return result;
        }

        public static IEnumerable<Task04Result> task_04(List<Project> data)
        {
            IEnumerable<Task04Result> result = data
                                                 .Select(p => p.Team).Distinct()
                                                 .GroupJoin(
                                                             data
                                                             .Select(p => p.Author)
                                                             .Union(
                                                                 data
                                                                 .SelectMany(p => p.Tasks)
                                                                 .Select(t => t.Performer))
                                                             .Distinct(),
                                                             t => t.Id,
                                                             u => u.TeamId,
                                                             (t, u) => new { Team = t, Users = u.ToList() }
                                                             )
                                                 .Where(t => t.Users.All(user => DateTime.Now.Year - user.BirthDay.Year > 10))
                                                 .Select(t => new Task04Result
                                                 {
                                                     Id = t.Team.Id,
                                                     Name = t.Team.Name,
                                                     Users = t.Users.OrderByDescending(user => user.RegisteredAt)
                                                 }
                                                 );

            return result;
        }

        public static IEnumerable<Task05Result> task_05(List<Project> data)
        {
            IOrderedEnumerable<Task05Result> result = data
                                                         .Select(p => p.Author)
                                                         .Union(data
                                                                .SelectMany(p => p.Tasks)
                                                                .Select(t => t.Performer))
                                                         .Distinct()
                                                         .GroupJoin(
                                                            data.SelectMany(p => p.Tasks),
                                                            user => user.Id,
                                                            task => task.Performer.Id,
                                                            (user, tasks) => new Task05Result  { User = user, Tasks = tasks = tasks.ToList().OrderByDescending(t => t.Name.Length)}
                                                          )
                                                         .OrderBy(u => u.User.FirstName);

            return result;
        }

        public static IEnumerable<Task06Result> task_06(List<Project> data, int userId)
        {
            IEnumerable<Task06Result> result = data
                                                 .Select(p => p.Author)
                                                 .Union(data
                                                        .SelectMany(p => p.Tasks)
                                                        .Select(t => t.Performer))
                                                 .Distinct()
                                                 .Where(u => u.Id == userId)
                                                 .GroupJoin(
                                                    data,
                                                    user => user.Id,
                                                    project => project.Author.Id,
                                                    (user, projects) => new { user, projects = projects.OrderByDescending(p => p.CreatedAt) }
                                                 )
                                                 .GroupJoin(
                                                    data.SelectMany(p => p.Tasks),
                                                    userInfo => userInfo.user.Id,
                                                    task => task.Performer.Id,
                                                    (userInfo, tasks) => new { userInfo.user, 
                                                                               userInfo.projects, 
                                                                               tasks = tasks.OrderByDescending(t => t.FinishedAt.HasValue ? (t.FinishedAt.Value - t.CreatedAt).Ticks : -1) }
                                                 ).
                                                 Select(info =>
                                                 {
                                                     var lastProject = info.projects.FirstOrDefault();
                                                     var lastProjectTasksCount = lastProject != null ? lastProject.Tasks.ToList().Count : 0;
                                                     return new Task06Result
                                                     {
                                                         User = info.user,
                                                         LastProject = lastProject,
                                                         LastProjectTasksCount = lastProjectTasksCount,
                                                         UnfinishedTasksCount = info.tasks.Where(t => (!t.FinishedAt.HasValue || t.State != 2)).ToList().Count,
                                                         LongestTask = info.tasks.FirstOrDefault()
                                                     };
                                                 });

            return result;
        }

        public static IEnumerable<Task07Result> task_07(List<Project> data)
        {
            IEnumerable<Task07Result> result = data
                                                 .GroupJoin(
                                                    data.Select(p => p.Author)
                                                        .Union(data
                                                                .SelectMany(p => p.Tasks)
                                                                .Select(t => t.Performer)
                                                        .Distinct()),
                                                    project => project.Team.Id,
                                                    user => user.TeamId,
                                                    (project, users) => new { project, Team = users }
                                                  )
                                                 .Select(info =>
                                                 {
                                                     var usersInTeamCount = (info.project.Description.Length > 20 
                                                                          || info.project.Tasks.ToList().Count < 3)
                                                                            ?
                                                                            (Nullable<int>) info.Team.ToList().Count
                                                                            :
                                                                            null;
                             
                                                        return
                                                        new Task07Result
                                                        {
                                                            Project = info.project,
                                                            LongestTask = info.project.Tasks.OrderByDescending(t => t.Description.Length).FirstOrDefault(),
                                                            ShortestTask = info.project.Tasks.OrderBy(t => t.Name.Length).FirstOrDefault(),
                                                            UsersInTeamCount = usersInTeamCount
                                                        };
                                                 });

            return result;
        }
    }
}
